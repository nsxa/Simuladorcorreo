-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-06-2014 a las 13:03:01
-- Versión del servidor: 5.5.36
-- Versión de PHP: 5.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `correo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos`
--

CREATE TABLE IF NOT EXISTS `correos` (
  `idCorreos` int(11) NOT NULL AUTO_INCREMENT,
  `Correo` varchar(45) NOT NULL,
  `Asunto` varchar(30) DEFAULT NULL,
  `Adjunto` varchar(45) DEFAULT NULL,
  `Leido` tinyint(1) NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `Destino` int(11) NOT NULL,
  `Destinatario` int(11) NOT NULL,
  PRIMARY KEY (`idCorreos`),
  KEY `fk_Correos_Usuarios_idx` (`Destino`),
  KEY `fk_Correos_Usuarios1_idx` (`Destinatario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `correos`
--

INSERT INTO `correos` (`idCorreos`, `Correo`, `Asunto`, `Adjunto`, `Leido`, `Fecha`, `Hora`, `Destino`, `Destinatario`) VALUES
(1, 'Hola', 'ad', 'c:/', 0, '2012-01-01', '15:00:00', 1, 1),
(2, 'Prueba Correo', 'Asunto', '', 0, '2014-06-04', '03:00:00', 2, 2),
(3, 'Correo 2', 'hola', 'c:/', 0, '0000-00-00', '13:00:00', 2, 2),
(5, 'Ivan', 'hola', '0', 0, '0000-00-00', '15:00:00', 2, 2),
(6, 'asdsaddas', 'sexto ', '', 0, '0000-00-00', '15:00:00', 1, 2),
(7, 'prueba', 'prueba', '', 0, '0000-00-00', '15:00:00', 7, 7),
(8, 'adsad', 'asd', '', 0, '0000-00-00', '15:00:00', 7, 6),
(9, 'aaa', 'Aa', '', 0, '0000-00-00', '15:00:00', 1, 6),
(10, 'asdsa', 'Ad', '', 0, '0000-00-00', '15:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuarios` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Apellido` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `Activado` tinyint(1) NOT NULL,
  PRIMARY KEY (`idUsuarios`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuarios`, `Nombre`, `Apellido`, `username`, `password`, `Activado`) VALUES
(1, 'Ivan', 'Martinez', 'alvmartinezg@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1),
(2, 'prueba', 'apel', 'prueba', '250cf8b51c773f3f8dc8b4be867a9a02', 1),
(3, 'tercero', 'ul', 'ivan@gmail.com', '2c42e5cf1cdbafea04ed267018ef1511', 1),
(4, 'Cuarto', 'ccc', 'Cuarto', '68053af2923e00204c3ca7c6a3150cf7', 1),
(5, 'cinco', 'cinco', 'cinco', '64e38e70efb84f926568d0522a105c33', 1),
(6, 'seis', 'seis', 'seis', '73e8e81b99c35fc5df48dfe1856c6d21', 1),
(7, 'prueba', 'prueba', 'prueba@gmail.com', 'c893bad68927b457dbed39460e6afd62', 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `correos`
--
ALTER TABLE `correos`
  ADD CONSTRAINT `fk_Correos_Usuarios` FOREIGN KEY (`Destino`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Correos_Usuarios1` FOREIGN KEY (`Destinatario`) REFERENCES `usuarios` (`idUsuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
