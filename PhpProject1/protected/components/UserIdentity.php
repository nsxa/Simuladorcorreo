<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    public function authenticate() {
        $user = Usuarios::model()->findByAttributes(array('username' => $this->username));
//        echo md5($this->password); echo '<br>';
//        echo $user->password; exit;
        if ($user === null) { // No user was found!
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        // $user->Password refers to the "password" column name from the database
        else if ((md5($this->password) . Yii::app()->params["salt"]) !== $user->password) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
// User/pass match
            $this->_id = $user->idUsuarios;
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }
 
    
}