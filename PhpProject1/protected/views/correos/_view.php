<?php
/* @var $this CorreosController */
/* @var $data Correos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCorreos')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idCorreos), array('view', 'id'=>$data->idCorreos)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Correo')); ?>:</b>
	<?php echo CHtml::encode($data->Correo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Asunto')); ?>:</b>
	<?php echo CHtml::encode($data->Asunto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Adjunto')); ?>:</b>
	<?php echo CHtml::encode($data->Adjunto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Leido')); ?>:</b>
	<?php echo CHtml::encode($data->Leido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Fecha')); ?>:</b>
	<?php echo CHtml::encode($data->Fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Hora')); ?>:</b>
	<?php echo CHtml::encode($data->Hora); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Destino')); ?>:</b>
	<?php echo CHtml::encode($data->Destino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Destinatario')); ?>:</b>
	<?php echo CHtml::encode($data->Destinatario); ?>
	<br />

	*/ ?>

</div>