<?php
/* @var $this CorreosController */
/* @var $model Correos */

$this->breadcrumbs=array(
	'Correoses'=>array('index'),
	$model->idCorreos,
);

$this->menu=array(
	array('label'=>'List Correos', 'url'=>array('index')),
	array('label'=>'Create Correos', 'url'=>array('create')),
	array('label'=>'Update Correos', 'url'=>array('update', 'id'=>$model->idCorreos)),
	array('label'=>'Delete Correos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idCorreos),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Correos', 'url'=>array('admin')),
);
?>

<h1>View Correos #<?php echo $model->idCorreos; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idCorreos',
		'Correo',
		'Asunto',
		'Adjunto',
		'Leido',
		'Fecha',
		'Hora',
		'Destino',
		'Destinatario',
	),
)); ?>
