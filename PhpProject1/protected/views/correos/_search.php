<?php
/* @var $this CorreosController */
/* @var $model Correos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idCorreos'); ?>
		<?php echo $form->textField($model,'idCorreos'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Correo'); ?>
		<?php echo $form->textField($model,'Correo',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Asunto'); ?>
		<?php echo $form->textField($model,'Asunto',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Adjunto'); ?>
		<?php echo $form->textField($model,'Adjunto',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Leido'); ?>
		<?php echo $form->textField($model,'Leido'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Fecha'); ?>
		<?php echo $form->textField($model,'Fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Hora'); ?>
		<?php echo $form->textField($model,'Hora'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Destino'); ?>
		<?php echo $form->textField($model,'Destino'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Destinatario'); ?>
		<?php echo $form->textField($model,'Destinatario'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->