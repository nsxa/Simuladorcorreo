<?php
/* @var $this CorreosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'bandeja de entrada',
);

$this->menu=array(
	array('label'=>'Nuevo Correo', 'url'=>array('create')),
	
);
?>

<h1>Bandeja de Entrada</h1>

<?php 
$arrays=$dataProvider->getData();
$cantidad=count($arrays);  



for($i=0;$i<$cantidad;$i++)
	{
		$valor=Yii::app()->db->createCommand('select * from usuarios where idUsuarios='.$arrays[$i]["Destinatario"])->queryAll();
	$arrays[$i]["Destinatario"]=$valor[0]["username"];
	}

$dataProvider->setData($arrays);
$dataProvider->setKeys(array("Correo"));

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'correos-grid',
	'summaryText' => 'mostrando {start}-{end} resultados de la pagina {page}',
        'dataProvider'=>$dataProvider,
        //'columns'=>array('Asunto,Correo,Adjunto,Fecha,Hora,Destinatario')  // this should not be necessary, since I put a select in the criteria.
		'columns'=>array(
		array(
              'header'=>'Correos',
              
			  
			  
			  
    'class' => 'CLinkColumn',
    'urlExpression'=>'"index.php?r=correos/view&id=".$data->idCorreos',
    'label' => 'Leer...',
			  ),
			array(
              'header'=>'Asunto',
              'name'=>'Asunto',
			  ),
			  array(
              'header'=>'Adjunto',
              'name'=>'Adjunto'
              ),
			  array(
              'header'=>'Fecha',
              'name'=>'Fecha'
              ),
			  array(
              'header'=>'Hora',
              'name'=>'Hora'
              ),
			  array(
              'header'=>'Destinatario',
              'name'=>'Destinatario'
              )
        )
		));
?>
