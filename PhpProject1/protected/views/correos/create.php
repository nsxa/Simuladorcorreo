<?php
/* @var $this CorreosController */
/* @var $model Correos */
/*
$this->breadcrumbs=array(
	'Correoses'=>array('index'),
	'Create',
);*/

$this->menu=array(
	array('label'=>'Bandeja de Entrada', 'url'=>array('index')),
	
);
?>

<h1>Nuevo Correo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>