<?php

/**
 * This is the model class for table "correos".
 *
 * The followings are the available columns in table 'correos':
 * @property integer $idCorreos
 * @property string $Correo
 * @property string $Asunto
 * @property string $Adjunto
 * @property integer $Leido
 * @property string $Fecha
 * @property string $Hora
 * @property integer $Destino
 * @property integer $Destinatario
 *
 * The followings are the available model relations:
 * @property Usuarios $destino
 * @property Usuarios $destinatario
 */
class Correos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'correos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('Correo, Leido, Fecha, Hora, Destino, Destinatario', 'required'),
			array('Correo,Asunto', 'required','message'=>'{attribute} no puede ser vacio.'),
			array('Destino', 'required','message'=>'El {attribute} no existe.'),
			array('Leido, Destino, Destinatario', 'numerical', 'integerOnly'=>true),
			array('Correo, Adjunto', 'length', 'max'=>45),
			array('Asunto', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idCorreos, Correo, Asunto, Adjunto, Leido, Fecha, Hora, Destino, Destinatario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'destino' => array(self::BELONGS_TO, 'Usuarios', 'Destino'),
			'destinatario' => array(self::BELONGS_TO, 'Usuarios', 'Destinatario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCorreos' => 'Id Correos',
			'Correo' => 'Correo',
			'Asunto' => 'Asunto',
			'Adjunto' => 'Adjunto',
			'Leido' => 'Leido',
			'Fecha' => 'Fecha',
			'Hora' => 'Hora',
			'Destino' => 'Destino',
			'Destinatario' => 'Destinatario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCorreos',$this->idCorreos);
		$criteria->compare('Correo',$this->Correo,true);
		$criteria->compare('Asunto',$this->Asunto,true);
		$criteria->compare('Adjunto',$this->Adjunto,true);
		$criteria->compare('Leido',$this->Leido);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('Hora',$this->Hora,true);
		$criteria->compare('Destino',$this->Destino);
		$criteria->compare('Destinatario',$this->Destinatario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Correos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
